/* 
 === don't forge to add style file to html ===
	<link rel="stylesheet" type="text/css" href="./dist/bundle.css">
*/

/* === quick try ===
import React from 'react';
import ReactDOM from 'react-dom';
import './main.css';
ReactDOM.render(
  <h1>Hello, world!</h1>,
  document.getElementById('root')
);
*/


var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var extractPlugin = new ExtractTextPlugin({
  filename: 'bundle.css'
});

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/dist'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['es2016' ,'react']
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        use: extractPlugin.extract({
          use: ['css-loader', 'sass-loader']
        })
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: "babel-loader"
          },
          {
            loader: "react-svg-loader",
            options: {
              jsx: true
            }
          }
        ]
      }
    ]
  },
  plugins: [
    extractPlugin
  ],
  watch: true,
};